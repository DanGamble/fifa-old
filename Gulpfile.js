'use strict';

// BASE SETUP
// =============================================================================

// -- Config
var config      = require('./config.json');

// -- Gulp
var gulp        = require('gulp');
var plugins     = require('gulp-load-plugins')();
var runSequence = require('run-sequence');

// CALL THE TASKS --------------------------------------------------------------
// -- JS
var scripts = require('./assets/gulp/scripts.js');

// -- Serve (Browser Sync)
var serve = require('./assets/gulp/serve.js');

// -- Styles
var styles = require('./assets/gulp/styles.js');

// BIND THE TASKS --------------------------------------------------------------

// -- JS
gulp.task('scripts:bundleJspm', scripts.bundleJspm(gulp, plugins, config));

// -- Serve
gulp.task('serve', serve.serve(gulp, config));

// -- Styles
gulp.task('styles', styles.styles(gulp, plugins, config));

// -- Default
gulp.task('default', function(cb) {
    runSequence('scripts:bundleJspm', 'serve', cb);
});
