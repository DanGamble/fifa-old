from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(
        r'^$',
        TemplateView.as_view(template_name='base.html'),
        name='homepage'
    ),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^clubs/', include('apps.clubs.urls', namespace='clubs')),

    url(r'^leagues/', include('apps.leagues.urls', namespace='leagues')),

    url(r'^nations/', include('apps.nations.urls', namespace='nations')),

    url(r'^players/', include('apps.players.urls', namespace='players')),

    url(r'^squads/', include('apps.squads.urls', namespace='squads')),

    url(r'^profiles/', include('apps.profiles.urls', namespace='profiles')),
)

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url("^404/$", TemplateView.as_view(template_name="404.html")),
        url("^500/$", TemplateView.as_view(template_name="500.html")),
    ) + static(
        settings.JSPM_URL, document_root=settings.JSPM_ROOT
    ) + static(
        settings.ASSET_URL, document_root=settings.ASSET_ROOT
    )
