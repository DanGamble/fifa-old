from django.views.generic import DetailView, ListView

from .models import League
from apps.core.functions.detailview_pagination import detailview_pagination
from apps.players.models import Player


class LeagueList(ListView):
    model = League
    paginate_by = 50


class LeagueDetail(DetailView):
    model = League

    def get_context_data(self, **kwargs):
        context = super(LeagueDetail, self).get_context_data()

        players = Player.objects.filter(
            league=self.get_object()
        ).select_related(
            'club',
            'league',
            'nation'
        )

        context['players'] = detailview_pagination(self, players)

        return context
