from django.contrib import admin

from apps.leagues.models import League


@admin.register(League)
class LeagueAdmin(admin.ModelAdmin):
    list_display = ['name', 'created']
