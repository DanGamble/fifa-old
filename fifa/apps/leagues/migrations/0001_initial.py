# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nations', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='League',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('asset_id', models.PositiveIntegerField(db_index=True, unique=True)),
                ('name', models.CharField(max_length=100, blank=True, null=True)),
                ('name_abbr', models.CharField(max_length=100, blank=True, null=True)),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('slug', models.CharField(max_length=50)),
                ('players_count', models.IntegerField(blank=True, null=True)),
                ('players_average_rating', models.IntegerField(blank=True, null=True)),
                ('players_inform', models.IntegerField(blank=True, null=True)),
                ('players_gold', models.IntegerField(blank=True, null=True)),
                ('players_silver', models.IntegerField(blank=True, null=True)),
                ('players_bronze', models.IntegerField(blank=True, null=True)),
                ('players_gk', models.IntegerField(blank=True, null=True)),
                ('players_def', models.IntegerField(blank=True, null=True)),
                ('players_mid', models.IntegerField(blank=True, null=True)),
                ('players_att', models.IntegerField(blank=True, null=True)),
                ('nation', models.ForeignKey(to_field='asset_id', null=True, to='nations.Nation', related_name='leagues', blank=True)),
            ],
            options={
                'verbose_name_plural': 'leagues',
                'verbose_name': 'league',
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
    ]
