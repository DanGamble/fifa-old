# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('leagues', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='league',
            options={'verbose_name_plural': 'leagues', 'verbose_name': 'league', 'ordering': ['-players_average_rating', 'name']},
        ),
        migrations.AlterField(
            model_name='league',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 15, 18, 12, 32, 21340, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='league',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 15, 18, 12, 36, 725498, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
