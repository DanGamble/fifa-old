from django.core.urlresolvers import reverse
from django.db import models


class League(models.Model):
    created = models.DateTimeField(
        auto_now_add=True
    )

    modified = models.DateTimeField(
        auto_now=True
    )

    asset_id = models.PositiveIntegerField(
        unique=True,
        db_index=True
    )

    name = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )

    name_abbr = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )

    nation = models.ForeignKey(
        'nations.Nation',
        to_field='asset_id',
        related_name='leagues',
        blank=True,
        null=True
    )

    slug = models.CharField(
        max_length=50
    )

    players_count = models.IntegerField(
        blank=True,
        null=True
    )

    players_average_rating = models.IntegerField(
        blank=True,
        null=True
    )

    players_inform = models.IntegerField(
        blank=True,
        null=True
    )

    players_gold = models.IntegerField(
        blank=True,
        null=True
    )

    players_silver = models.IntegerField(
        blank=True,
        null=True
    )

    players_bronze = models.IntegerField(
        blank=True,
        null=True
    )

    players_gk = models.IntegerField(
        blank=True,
        null=True
    )

    players_def = models.IntegerField(
        blank=True,
        null=True
    )

    players_mid = models.IntegerField(
        blank=True,
        null=True
    )

    players_att = models.IntegerField(
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-players_average_rating', 'name']
        verbose_name = "league"
        verbose_name_plural = "leagues"

    def get_absolute_url(self):
        return reverse('leagues:league_detail', kwargs={'slug': self.slug})

