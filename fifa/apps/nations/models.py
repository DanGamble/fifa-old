from django.core.urlresolvers import reverse
from django.db import models


class Nation(models.Model):
    created = models.DateTimeField(
        auto_now_add=True
    )

    modified = models.DateTimeField(
        auto_now=True
    )

    asset_id = models.PositiveIntegerField(
        unique=True,
        db_index=True
    )

    slug = models.SlugField(
        max_length=50
    )

    name = models.CharField(
        max_length=100
    )

    name_abbr = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )

    players_count = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_average_rating = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_inform = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_gold = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_silver = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_bronze = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_gk = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_def = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_mid = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_att = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-players_average_rating', 'name']
        verbose_name = "nation"
        verbose_name_plural = "nations"

    def get_absolute_url(self):
        return reverse('nations:nation_detail', kwargs={'slug': self.slug})
