from django.views.generic import ListView, DetailView

from apps.core.functions.detailview_pagination import detailview_pagination
from apps.players.models import Player
from .models import Nation


class NationList(ListView):
    model = Nation
    paginate_by = 50


class NationDetail(DetailView):
    model = Nation

    def get_context_data(self, **kwargs):
        context = super(NationDetail, self).get_context_data()

        players = Player.objects.filter(
            nation=self.get_object()
        ).select_related(
            'club',
            'league',
            'nation'
        )

        context['players'] = detailview_pagination(self, players)

        return context
