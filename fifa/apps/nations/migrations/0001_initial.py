# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Nation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('asset_id', models.PositiveIntegerField(db_index=True, unique=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('slug', models.SlugField()),
                ('name', models.CharField(max_length=100)),
                ('name_abbr', models.CharField(null=True, max_length=100, blank=True)),
                ('players_count', models.PositiveIntegerField(null=True, blank=True)),
                ('players_average_rating', models.PositiveIntegerField(null=True, blank=True)),
                ('players_inform', models.PositiveIntegerField(null=True, blank=True)),
                ('players_gold', models.PositiveIntegerField(null=True, blank=True)),
                ('players_silver', models.PositiveIntegerField(null=True, blank=True)),
                ('players_bronze', models.PositiveIntegerField(null=True, blank=True)),
                ('players_gk', models.PositiveIntegerField(null=True, blank=True)),
                ('players_def', models.PositiveIntegerField(null=True, blank=True)),
                ('players_mid', models.PositiveIntegerField(null=True, blank=True)),
                ('players_att', models.PositiveIntegerField(null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'nations',
                'ordering': ['name'],
                'verbose_name': 'nation',
            },
            bases=(models.Model,),
        ),
    ]
