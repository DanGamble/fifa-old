# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nations', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='nation',
            options={'verbose_name_plural': 'nations', 'verbose_name': 'nation', 'ordering': ['-players_average_rating', 'name']},
        ),
    ]
