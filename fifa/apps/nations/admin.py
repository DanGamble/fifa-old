from django.contrib import admin

from apps.nations.models import Nation


@admin.register(Nation)
class NationAdmin(admin.ModelAdmin):
    list_display = ['name', 'asset_id', 'created']
