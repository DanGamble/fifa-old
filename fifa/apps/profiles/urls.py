from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(
        r'^login/$',
        'django.contrib.auth.views.login',
        {
            'template_name': 'profiles/login.html'
        },
        name='login'
    ),

    url(
        r'^logout/$',
        'django.contrib.auth.views.logout',
        {
            'next_page': '/'
        },
        name='logout'
    ),

    # url(
    #     r'^register/$',
    #     'django.contrib.auth.views.r'
    # ),

    url(
        r'^password-change/$',
        'django.contrib.auth.views.password_change',
        {
            'post_change_redirect': '/profiles/password-change/success/'
        },
        name='password-change'
    ),

    url(
        r'^password-change/success/$',
        'django.contrib.auth.views.password_change_done',
        name='password-change-success'
    ),

    url(
        r'^password-reset/$',
        'django.contrib.auth.views.password_reset',
        {
            'post_reset_redirect': '/profiles/password-reset/done/'
        },
        name='password-reset'
    ),

    url(
        r'^password-reset/next/$',
        'django.contrib.auth.views.password_reset_done',
        name='password-reset-done'
    ),

    url(
        r'^password-reset/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        'django.contrib.auth.views.password_reset_confirm',
        name='password_reset_confirm'
    )

    # TODO: Password reset urls
)
