from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager
)
from django.db import models
from django.utils import timezone


class ProfileManager(BaseUserManager):
    def create_user(self, email, password=None, **kwargs):
        if not email:
            raise ValueError('Users must have a valid email address.')

        if not kwargs.get('display_name'):
            raise ValueError('Users must have a valid display name.')

        profile = self.model(
            email=self.normalize_email(email),
            display_name=kwargs.get('display_name')
        )

        profile.set_password(password)
        profile.save(using=self._db)

        return profile

    def create_superuser(self, email, password, **kwargs):
        profile = self.create_user(email, password, **kwargs)

        profile.is_admin = True
        profile.is_staff = True
        profile.save(using=self._db)

        return profile


class Profile(AbstractBaseUser):
    email = models.EmailField(
        max_length=100,
        unique=True
    )

    display_name = models.CharField(
        max_length=32,
        unique=True
    )

    first_name = models.CharField(
        max_length=40,
        blank=True
    )

    last_name = models.CharField(
        max_length=40,
        blank=True
    )

    bio = models.TextField(
        max_length=140,
        blank=True
    )

    is_admin = models.BooleanField(
        default=False
    )

    is_staff = models.BooleanField(
        default=False
    )

    is_active = models.BooleanField(
        default=True
    )

    # last_login = models.DateTimeField(
    #     'last login',
    #     default=timezone.now
    # )

    created_at = models.DateTimeField(
        auto_now_add=True
    )

    updated_at = models.DateTimeField(
        auto_now=True
    )

    objects = ProfileManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['display_name']

    def __str__(self):
        return self.display_name

    def get_full_name(self):
        return '{} {}'.join(
            self.first_name,
            self.last_name
        )

    def get_short_name(self):
        return self.first_name

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True
