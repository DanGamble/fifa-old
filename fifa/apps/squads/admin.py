from django.contrib import admin

from .models import Squad


@admin.register(Squad)
class ClubAdmin(admin.ModelAdmin):
    raw_id_fields = (
        'squad_1',
        'squad_2',
        'squad_3',
        'squad_4',
        'squad_5',
        'squad_6',
        'squad_7',
        'squad_8',
        'squad_9',
        'squad_10',
        'squad_11',
        'bench_1',
        'bench_2',
        'bench_3',
        'bench_4',
        'bench_5',
        'bench_6',
        'bench_7'
    )
    list_display = ['name', 'formation', 'user', 'created']
    readonly_fields = ['squad_rating', 'slug']

