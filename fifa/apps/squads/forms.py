from django import forms

from .models import Squad
from apps.players.models import Player


class SquadBuilderForm(forms.ModelForm):

    squad_1_form = forms.IntegerField(
        required=False
    )

    squad_1_chemistry_form = forms.IntegerField(
        required=False,
        min_value=0,
        max_value=10
    )

    squad_1_chemistry_style_form = forms.CharField(
        required=False
    )

    squad_1_loyalty_form = forms.BooleanField(
        required=False
    )

    squad_2_form = forms.IntegerField(
        required=False
    )

    squad_2_chemistry_form = forms.IntegerField(
        required=False,
        min_value=1,
        max_value=11
    )

    squad_2_chemistry_style_form = forms.CharField(
        required=False
    )

    squad_2_loyalty_form = forms.BooleanField(
        required=False
    )

    squad_3_form = forms.IntegerField(
        required=False
    )

    squad_3_chemistry_form = forms.IntegerField(
        required=False,
        min_value=2,
        max_value=12
    )

    squad_3_chemistry_style_form = forms.CharField(
        required=False
    )

    squad_3_loyalty_form = forms.BooleanField(
        required=False
    )

    squad_4_form = forms.IntegerField(
        required=False
    )

    squad_4_chemistry_form = forms.IntegerField(
        required=False,
        min_value=3,
        max_value=13
    )

    squad_4_chemistry_style_form = forms.CharField(
        required=False
    )

    squad_4_loyalty_form = forms.BooleanField(
        required=False
    )

    squad_5_form = forms.IntegerField(
        required=False
    )

    squad_5_chemistry_form = forms.IntegerField(
        required=False,
        min_value=4,
        max_value=14
    )

    squad_5_chemistry_style_form = forms.CharField(
        required=False
    )

    squad_5_loyalty_form = forms.BooleanField(
        required=False
    )

    squad_6_form = forms.IntegerField(
        required=False
    )

    squad_6_chemistry_form = forms.IntegerField(
        required=False,
        min_value=5,
        max_value=15
    )

    squad_6_chemistry_style_form = forms.CharField(
        required=False
    )

    squad_6_loyalty_form = forms.BooleanField(
        required=False
    )

    squad_7_form = forms.IntegerField(
        required=False
    )

    squad_7_chemistry_form = forms.IntegerField(
        required=False,
        min_value=6,
        max_value=16
    )

    squad_7_chemistry_style_form = forms.CharField(
        required=False
    )

    squad_7_loyalty_form = forms.BooleanField(
        required=False
    )

    squad_8_form = forms.IntegerField(
        required=False
    )

    squad_8_chemistry_form = forms.IntegerField(
        required=False,
        min_value=7,
        max_value=17
    )

    squad_8_chemistry_style_form = forms.CharField(
        required=False
    )

    squad_8_loyalty_form = forms.BooleanField(
        required=False
    )

    squad_9_form = forms.IntegerField(
        required=False
    )

    squad_9_chemistry_form = forms.IntegerField(
        required=False,
        min_value=8,
        max_value=18
    )

    squad_9_chemistry_style_form = forms.CharField(
        required=False
    )

    squad_9_loyalty_form = forms.BooleanField(
        required=False
    )

    squad_10_form = forms.IntegerField(
        required=False
    )

    squad_10_chemistry_form = forms.IntegerField(
        required=False,
        min_value=9,
        max_value=19
    )

    squad_10_chemistry_style_form = forms.CharField(
        required=False
    )

    squad_10_loyalty_form = forms.BooleanField(
        required=False
    )

    squad_11_form = forms.IntegerField(
        required=False
    )

    squad_11_chemistry_form = forms.IntegerField(
        required=False,
        min_value=10,
        max_value=20
    )

    squad_11_chemistry_style_form = forms.CharField(
        required=False
    )

    squad_11_loyalty_form = forms.BooleanField(
        required=False
    )

    class Meta:
        model = Squad
        exclude = [
            'squad_1',
            'squad_1_position',
            'squad_1_chemistry',
            'squad_1_chemistry_style',
            'squad_1_loyalty',

            'squad_2',
            'squad_2_position',
            'squad_2_chemistry',
            'squad_2_chemistry_style',
            'squad_2_loyalty',

            'squad_3',
            'squad_3_position',
            'squad_3_chemistry',
            'squad_3_chemistry_style',
            'squad_3_loyalty',

            'squad_4',
            'squad_4_position',
            'squad_4_chemistry',
            'squad_4_chemistry_style',
            'squad_4_loyalty',

            'squad_5',
            'squad_5_position',
            'squad_5_chemistry',
            'squad_5_chemistry_style',
            'squad_5_loyalty',

            'squad_6',
            'squad_6_position',
            'squad_6_chemistry',
            'squad_6_chemistry_style',
            'squad_6_loyalty',

            'squad_7',
            'squad_7_position',
            'squad_7_chemistry',
            'squad_7_chemistry_style',
            'squad_7_loyalty',

            'squad_8',
            'squad_8_position',
            'squad_8_chemistry',
            'squad_8_chemistry_style',
            'squad_8_loyalty',

            'squad_9',
            'squad_9_position',
            'squad_9_chemistry',
            'squad_9_chemistry_style',
            'squad_9_loyalty',

            'squad_10',
            'squad_10_position',
            'squad_10_chemistry',
            'squad_10_chemistry_style',
            'squad_10_loyalty',

            'squad_11',
            'squad_11_position',
            'squad_11_chemistry',
            'squad_11_chemistry_style',
            'squad_11_loyalty',

            'bench_1',
            'bench_2',
            'bench_3',
            'bench_4',
            'bench_5',
            'bench_6',
            'bench_7',

            'created',
            'updated',
            'slug',
            'squad_rating',
            'special',
            'user',
        ]
