import statistics
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.text import slugify
from apps.players.models import Player
from apps.profiles.models import Profile

FORMATIONS = (
    ('3412', '3-4-1-2'),
    ('3421', '3-4-2-1'),
    ('343', '3-4-3'),
    ('352', '3-5-2'),
    ('41212-1', '4-1-2-1-2'),
    ('41212-2', '4-1-2-1-2 (2)'),
    ('4141', '4-1-4-1'),
    ('4231-1', '4-2-3-1'),
    ('4231-2', '4-2-3-1 (2)'),
    ('4222', '4-2-2-2'),
    ('4312', '4-3-1-2'),
    ('4321', '4-3-2-1'),
    ('433-1', '4-3-3 (1)'),
    ('433-2', '4-3-3 (2)'),
    ('433-3', '4-3-3 (3)'),
    ('433-4', '4-3-3 (4)'),
    ('433-5', '4-3-3 (5)'),
    ('4411', '4-4-1-1'),
    ('442-1', '4-4-2 (1)'),
    ('442-2', '4-4-2 (2)'),
    ('451-1', '4-5-1 (1)'),
    ('451-2', '4-5-1 (2)'),
    ('5212', '5-2-1-2'),
    ('5221', '5-2-2-1'),
    ('532', '5-3-2'),
)


class Squad(models.Model):
    created = models.DateTimeField(
        auto_now_add=True
    )

    updated = models.DateTimeField(
        auto_now=True
    )

    name = models.CharField(
        max_length=255
    )

    slug = models.SlugField(
        unique=True
    )

    formation = models.CharField(
        max_length=10,
        choices=FORMATIONS
    )

    squad_rating = models.PositiveIntegerField(
        default=0
    )

    special = models.BooleanField(
        default=False
    )

    generate_image = models.BooleanField(
        default=False
    )

    user = models.ForeignKey(
        Profile,
        blank=True,
        null=True
    )

    # manager = models.ForeignKey(
    #     Manager
    # )

    squad_1 = models.ForeignKey(Player, related_name='squad_1', blank=True, null=True)
    squad_1_position = models.CharField(max_length=10, blank=True, null=True)
    squad_1_chemistry = models.CharField(max_length=2, blank=True, null=True)
    squad_1_chemistry_style = models.CharField(max_length=10, blank=True, null=True)
    squad_1_loyalty = models.BooleanField(default=False)

    squad_2 = models.ForeignKey(Player, related_name='squad_2', blank=True, null=True)
    squad_2_position = models.CharField(max_length=10, blank=True, null=True)
    squad_2_chemistry = models.CharField(max_length=2, blank=True, null=True)
    squad_2_chemistry_style = models.CharField(max_length=10, blank=True, null=True)
    squad_2_loyalty = models.BooleanField(default=False)

    squad_3 = models.ForeignKey(Player, related_name='squad_3', blank=True, null=True)
    squad_3_position = models.CharField(max_length=10, blank=True, null=True)
    squad_3_chemistry = models.CharField(max_length=2, blank=True, null=True)
    squad_3_chemistry_style = models.CharField(max_length=10, blank=True, null=True)
    squad_3_loyalty = models.BooleanField(default=False)

    squad_4 = models.ForeignKey(Player, related_name='squad_4', blank=True, null=True)
    squad_4_position = models.CharField(max_length=10, blank=True, null=True)
    squad_4_chemistry = models.CharField(max_length=2, blank=True, null=True)
    squad_4_chemistry_style = models.CharField(max_length=10, blank=True, null=True)
    squad_4_loyalty = models.BooleanField(default=False)

    squad_5 = models.ForeignKey(Player, related_name='squad_5', blank=True, null=True)
    squad_5_position = models.CharField(max_length=10, blank=True, null=True)
    squad_5_chemistry = models.CharField(max_length=2, blank=True, null=True)
    squad_5_chemistry_style = models.CharField(max_length=10, blank=True, null=True)
    squad_5_loyalty = models.BooleanField(default=False)

    squad_6 = models.ForeignKey(Player, related_name='squad_6', blank=True, null=True)
    squad_6_position = models.CharField(max_length=10, blank=True, null=True)
    squad_6_chemistry = models.CharField(max_length=2, blank=True, null=True)
    squad_6_chemistry_style = models.CharField(max_length=10, blank=True, null=True)
    squad_6_loyalty = models.BooleanField(default=False)

    squad_7 = models.ForeignKey(Player, related_name='squad_7', blank=True, null=True)
    squad_7_position = models.CharField(max_length=10, blank=True, null=True)
    squad_7_chemistry = models.CharField(max_length=2, blank=True, null=True)
    squad_7_chemistry_style = models.CharField(max_length=10, blank=True, null=True)
    squad_7_loyalty = models.BooleanField(default=False)

    squad_8 = models.ForeignKey(Player, related_name='squad_8', blank=True, null=True)
    squad_8_position = models.CharField(max_length=10, blank=True, null=True)
    squad_8_chemistry = models.CharField(max_length=2, blank=True, null=True)
    squad_8_chemistry_style = models.CharField(max_length=10, blank=True, null=True)
    squad_8_loyalty = models.BooleanField(default=False)

    squad_9 = models.ForeignKey(Player, related_name='squad_9', blank=True, null=True)
    squad_9_position = models.CharField(max_length=10, blank=True, null=True)
    squad_9_chemistry = models.CharField(max_length=2, blank=True, null=True)
    squad_9_chemistry_style = models.CharField(max_length=10, blank=True, null=True)
    squad_9_loyalty = models.BooleanField(default=False)

    squad_10 = models.ForeignKey(Player, related_name='squad_10', blank=True, null=True)
    squad_10_position = models.CharField(max_length=10, blank=True, null=True)
    squad_10_chemistry = models.CharField(max_length=2, blank=True, null=True)
    squad_10_chemistry_style = models.CharField(max_length=10, blank=True, null=True)
    squad_10_loyalty = models.BooleanField(default=False)

    squad_11 = models.ForeignKey(Player, related_name='squad_11', blank=True, null=True)
    squad_11_position = models.CharField(max_length=10, blank=True, null=True)
    squad_11_chemistry = models.CharField(max_length=2, blank=True, null=True)
    squad_11_chemistry_style = models.CharField(max_length=10, blank=True, null=True)
    squad_11_loyalty = models.BooleanField(default=False)

    bench_1 = models.ForeignKey(Player, related_name='bench_1', blank=True, null=True)
    bench_2 = models.ForeignKey(Player, related_name='bench_2', blank=True, null=True)
    bench_3 = models.ForeignKey(Player, related_name='bench_3', blank=True, null=True)
    bench_4 = models.ForeignKey(Player, related_name='bench_4', blank=True, null=True)
    bench_5 = models.ForeignKey(Player, related_name='bench_5', blank=True, null=True)
    bench_6 = models.ForeignKey(Player, related_name='bench_6', blank=True, null=True)
    bench_7 = models.ForeignKey(Player, related_name='bench_7', blank=True, null=True)

    def __str__(self):
        return self.name

    def save(self, commit=False, *args, **kwargs):
        try:
            self.squad_rating = statistics.mean(self.player_ratings())
        except statistics.StatisticsError:
            self.squad_rating = 0
        except Exception as e:
            print(e)

        try:
            self.user = self.request.user
        except Exception as e:
            print(e)

        self.slug = '{}-{}'.format(
            self.id,
            slugify(self.name)
        )

        if not commit:
            self.save(commit=True)

        super(Squad, self).save(*args, **kwargs)

    class Meta:
        ordering = ('created', 'name')
        verbose_name = 'Squad'
        verbose_name_plural = 'Squads'

    def get_absolute_url(self):
        return reverse('squads:squad_detail', kwargs={'slug': self.slug})

    def players(self):
        return [
            self.squad_1,
            self.squad_2,
            self.squad_3,
            self.squad_4,
            self.squad_5,
            self.squad_6,
            self.squad_7,
            self.squad_8,
            self.squad_9,
            self.squad_10,
            self.squad_11,
            self.bench_1,
            self.bench_2,
            self.bench_3,
            self.bench_4,
            self.bench_5,
            self.bench_6,
            self.bench_7
        ]

    def player_ratings(self):
        return [x.overall_rating for x in self.players() if x is not None]
