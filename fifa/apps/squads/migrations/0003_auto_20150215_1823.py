# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('squads', '0002_auto_20150215_1812'),
    ]

    operations = [
        migrations.AlterField(
            model_name='squad',
            name='player_12',
            field=models.ForeignKey(to='players.Player', null=True, related_name='player_12', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='squad',
            name='player_13',
            field=models.ForeignKey(to='players.Player', null=True, related_name='player_13', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='squad',
            name='player_14',
            field=models.ForeignKey(to='players.Player', null=True, related_name='player_14', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='squad',
            name='player_15',
            field=models.ForeignKey(to='players.Player', null=True, related_name='player_15', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='squad',
            name='player_16',
            field=models.ForeignKey(to='players.Player', null=True, related_name='player_16', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='squad',
            name='player_17',
            field=models.ForeignKey(to='players.Player', null=True, related_name='player_17', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='squad',
            name='player_18',
            field=models.ForeignKey(to='players.Player', null=True, related_name='player_18', blank=True),
            preserve_default=True,
        ),
    ]
