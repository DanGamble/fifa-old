# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Squad',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=255)),
                ('formation', models.CharField(choices=[('3412', '3-4-1-2'), ('3421', '3-4-2-1'), ('343', '3-4-3'), ('352', '3-5-2'), ('41212-1', '4-1-2-1-2'), ('41212-2', '4-1-2-1-2 (2)'), ('4141', '4-1-4-1'), ('4231-1', '4-2-3-1'), ('4231-2', '4-2-3-1 (2)'), ('4222', '4-2-2-2'), ('4312', '4-3-1-2'), ('4321', '4-3-2-1'), ('433-1', '4-3-3 (1)'), ('433-2', '4-3-3 (2)'), ('433-3', '4-3-3 (3)'), ('433-4', '4-3-3 (4)'), ('433-5', '4-3-3 (5)'), ('4411', '4-4-1-1'), ('442-1', '4-4-2 (1)'), ('442-2', '4-4-2 (2)'), ('451-1', '4-5-1 (1)'), ('451-2', '4-5-1 (2)'), ('5212', '5-2-1-2'), ('5221', '5-2-2-1'), ('532', '5-3-2')], max_length=10)),
                ('squad_rating', models.PositiveIntegerField(default=0)),
                ('special', models.BooleanField(default=False)),
                ('generate_image', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('player_1', models.ForeignKey(to='players.Player', related_name='player_1')),
                ('player_10', models.ForeignKey(to='players.Player', related_name='player_10')),
                ('player_11', models.ForeignKey(to='players.Player', related_name='player_11')),
                ('player_12', models.ForeignKey(to='players.Player', related_name='player_12')),
                ('player_13', models.ForeignKey(to='players.Player', related_name='player_13')),
                ('player_14', models.ForeignKey(to='players.Player', related_name='player_14')),
                ('player_15', models.ForeignKey(to='players.Player', related_name='player_15')),
                ('player_16', models.ForeignKey(to='players.Player', related_name='player_16')),
                ('player_17', models.ForeignKey(to='players.Player', related_name='player_17')),
                ('player_18', models.ForeignKey(to='players.Player', related_name='player_18')),
                ('player_2', models.ForeignKey(to='players.Player', related_name='player_2')),
                ('player_3', models.ForeignKey(to='players.Player', related_name='player_3')),
                ('player_4', models.ForeignKey(to='players.Player', related_name='player_4')),
                ('player_5', models.ForeignKey(to='players.Player', related_name='player_5')),
                ('player_6', models.ForeignKey(to='players.Player', related_name='player_6')),
                ('player_7', models.ForeignKey(to='players.Player', related_name='player_7')),
                ('player_8', models.ForeignKey(to='players.Player', related_name='player_8')),
                ('player_9', models.ForeignKey(to='players.Player', related_name='player_9')),
                ('user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Squads',
                'verbose_name': 'Squad',
                'ordering': ('created_at', 'name'),
            },
            bases=(models.Model,),
        ),
    ]
