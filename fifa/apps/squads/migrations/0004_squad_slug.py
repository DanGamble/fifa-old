# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('squads', '0003_auto_20150215_1823'),
    ]

    operations = [
        migrations.AddField(
            model_name='squad',
            name='slug',
            field=models.SlugField(default='1-squad', unique=True),
            preserve_default=False,
        ),
    ]
