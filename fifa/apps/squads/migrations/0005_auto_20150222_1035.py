# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0002_auto_20150216_1407'),
        ('squads', '0004_squad_slug'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='squad',
            name='player_1',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_10',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_11',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_12',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_13',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_14',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_15',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_16',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_17',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_18',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_2',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_3',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_4',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_5',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_6',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_7',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_8',
        ),
        migrations.RemoveField(
            model_name='squad',
            name='player_9',
        ),
        migrations.AddField(
            model_name='squad',
            name='bench_1',
            field=models.ForeignKey(null=True, blank=True, related_name='bench_1', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='bench_2',
            field=models.ForeignKey(null=True, blank=True, related_name='bench_2', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='bench_3',
            field=models.ForeignKey(null=True, blank=True, related_name='bench_3', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='bench_4',
            field=models.ForeignKey(null=True, blank=True, related_name='bench_4', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='bench_5',
            field=models.ForeignKey(null=True, blank=True, related_name='bench_5', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='bench_6',
            field=models.ForeignKey(null=True, blank=True, related_name='bench_6', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='bench_7',
            field=models.ForeignKey(null=True, blank=True, related_name='bench_7', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_1',
            field=models.ForeignKey(null=True, blank=True, related_name='squad_1', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_10',
            field=models.ForeignKey(null=True, blank=True, related_name='squad_10', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_10_chemistry',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_10_chemistry_style',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_10_loyalty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_10_position',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_11',
            field=models.ForeignKey(null=True, blank=True, related_name='squad_11', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_11_chemistry',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_11_chemistry_style',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_11_loyalty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_11_position',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_1_chemistry',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_1_chemistry_style',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_1_loyalty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_1_position',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_2',
            field=models.ForeignKey(null=True, blank=True, related_name='squad_2', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_2_chemistry',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_2_chemistry_style',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_2_loyalty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_2_position',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_3',
            field=models.ForeignKey(null=True, blank=True, related_name='squad_3', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_3_chemistry',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_3_chemistry_style',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_3_loyalty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_3_position',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_4',
            field=models.ForeignKey(null=True, blank=True, related_name='squad_4', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_4_chemistry',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_4_chemistry_style',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_4_loyalty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_4_position',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_5',
            field=models.ForeignKey(null=True, blank=True, related_name='squad_5', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_5_chemistry',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_5_chemistry_style',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_5_loyalty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_5_position',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_6',
            field=models.ForeignKey(null=True, blank=True, related_name='squad_6', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_6_chemistry',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_6_chemistry_style',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_6_loyalty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_6_position',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_7',
            field=models.ForeignKey(null=True, blank=True, related_name='squad_7', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_7_chemistry',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_7_chemistry_style',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_7_loyalty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_7_position',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_8',
            field=models.ForeignKey(null=True, blank=True, related_name='squad_8', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_8_chemistry',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_8_chemistry_style',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_8_loyalty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_8_position',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_9',
            field=models.ForeignKey(null=True, blank=True, related_name='squad_9', to='players.Player'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_9_chemistry',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_9_chemistry_style',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_9_loyalty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='squad',
            name='squad_9_position',
            field=models.CharField(max_length=10, null=True, blank=True),
            preserve_default=True,
        ),
    ]
