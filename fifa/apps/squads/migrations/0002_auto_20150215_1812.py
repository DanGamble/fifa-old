# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('squads', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='squad',
            options={'verbose_name_plural': 'Squads', 'verbose_name': 'Squad', 'ordering': ('created', 'name')},
        ),
        migrations.RenameField(
            model_name='squad',
            old_name='created_at',
            new_name='created',
        ),
        migrations.RenameField(
            model_name='squad',
            old_name='updated_at',
            new_name='updated',
        ),
    ]
