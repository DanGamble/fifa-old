# Core imports
from django.conf.urls import patterns, url

from .views import SquadList, SquadDetail, SquadBuilder


urlpatterns = patterns(
    '',
    url(r'^$', SquadList.as_view(), name='squad_list'),
    url(r'^builder/$', SquadBuilder.as_view(), name='squad_builder'),
    url(r'^(?P<slug>[a-z0-9\-]+)/$', SquadDetail.as_view(), name='squad_detail'),
)
