from django.views.generic import ListView, DetailView, CreateView

from .models import Squad
from .forms import SquadBuilderForm
from apps.players.models import Player


class SquadList(ListView):
    model = Squad
    paginate_by = 25


class SquadDetail(DetailView):
    model = Squad

    # TODO: Reduce the number of queries, it's quick but surely we can reduce them?


class SquadBuilder(CreateView):
    form_class = SquadBuilderForm
    template_name = 'squads/squad_builder.html'

    def form_valid(self, form):
        obj = form.save(commit=False)

        if self.request.user.is_authenticated():
            obj.user = self.request.user
        else:
            # TODO: Potentially save squad to session and assign it to their
            # account if they make one in the session
            self.request.session['squad'] = obj

        form_players = {
            'squad_1': form.data['squad_1_form'],
            'squad_2': form.data['squad_2_form'],
            'squad_3': form.data['squad_3_form'],
            'squad_4': form.data['squad_4_form'],
            'squad_5': form.data['squad_5_form'],
            'squad_6': form.data['squad_6_form'],
            'squad_7': form.data['squad_7_form'],
            'squad_8': form.data['squad_8_form'],
            'squad_9': form.data['squad_9_form'],
            'squad_10': form.data['squad_10_form'],
            'squad_11': form.data['squad_11_form'],
        }

        fields = {}
        for num in range(1, 12):
            fields['squad_{}'.format(num)] = [
                'squad_{}_position'.format(num),
                'squad_{}_chemistry'.format(num),
                'squad_{}_chemistry_style'.format(num),
                'squad_{}_loyalty'.format(num),
            ]

        submitted_players = {k: v for k, v in form_players.items() if v != ''}

        if submitted_players:
            players = [player for player in Player.objects.filter(
                id__in=submitted_players.values()
            )]

            for k, v in submitted_players.items():
                for player in players:
                    if int(v) == int(player.id):
                        setattr(obj, k, player)
                        setattr(obj, '{}_position'.format(k), player.position)
                        setattr(obj, '{}_chemistry'.format(k), form.data['{}_chemistry_form'.format(k)])
                        setattr(obj, '{}_chemistry_style'.format(k), form.data['{}_chemistry_style_form'.format(k)])
                        setattr(obj, '{}_loyalty'.format(k), form.data.get('{}_loyalty_form'.format(k), ''))

        obj.save()

        return super(SquadBuilder, self).form_valid(form)
