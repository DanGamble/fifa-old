from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormMixin, CreateView

from .forms import PlayerFilterForm
from .models import Player


class SortMixin(object):
    pass


class PlayerViewMixin(object):
    model = Player

    def get_queryset(self):
        return super(PlayerViewMixin, self).get_queryset().prefetch_related(
            'club',
            'league',
            'nation'
        )


class PlayerList(PlayerViewMixin, FormMixin, ListView):
    paginate_by = 50
    template_name = 'players/player_list.html'

    def get_context_data(self, **kwargs):
        context = super(PlayerList, self).get_context_data()

        if self.request.GET:
            context['form'] = PlayerFilterForm(self.request.GET)
        else:
            context['form'] = PlayerFilterForm()

        return context

    def get_queryset(self):
        qs = super(PlayerList, self).get_queryset()
        parameters = {
            'rating_min': {
                'overall_rating__gte': self.request.GET.get('rating_min')
            },
            'rating_max': {
                'overall_rating__lte': self.request.GET.get('rating_max')
            },
            'player_type': {
                'player_type': self.request.GET.get('player_type')
            },
            'skill_min': {
                'skill_moves__gte': self.request.GET.get('skill_min')
            },
            'skill_max': {
                'skill_moves__lte': self.request.GET.get('skill_max')
            },
            'weak_min': {
                'weak_foot__gte': self.request.GET.get('weak_min')
            },
            'weak_max': {
                'weak_foot__lte': self.request.GET.get('weak_max')
            },
            'workrate_def': {
                'workrate_def': self.request.GET.get('workrate_def')
            },
            'workrate_att': {
                'workrate_att': self.request.GET.get('workrate_att')
            },
            'preferred_foot': {
                'preferred_foot': self.request.GET.get('preferred_foot')
            },
            # 'pricesplatform': self.request.GET.get('prices_platform'),
            # 'pricesmin': self.request.GET.get('prices_min'),
            # 'pricesmax': self.request.GET.get('prices_max'),
            # 'clubs': self.request.GET.get('clubs'),
            # 'leagues': self.request.GET.get('leagues'),
            # 'nations': self.request.GET.get('nations')
        }

        params = filter(lambda x: self.request.GET.get(x) != 'all', parameters)

        if self.request.GET:
            for param in params:
                qs = qs.filter(
                    **parameters[param]
                )

        return qs


class PlayerDetail(PlayerViewMixin, DetailView):
    pass
