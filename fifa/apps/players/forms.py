from django import forms

from .models import Player
from ..clubs.models import Club
from ..leagues.models import League
from ..nations.models import Nation

default_all_choice = [
    ('all', 'All')
]

small_range_choices = [
    (x, x) for x in range(1, 6)
]

large_range_choices = [
    (x, x) for x in range(1, 100)
]

workrate_choices = default_all_choice + [
    ('high', 'High'),
    ('med', 'Medium'),
    ('low', 'Low')
]

prices_platform_choices = [
    ('xbox', 'Xbox'),
    ('psn', 'PSN'),
    ('pc', 'PC')
]

position_choices = default_all_choice + [
    ('gk', 'GK'),
    ('rwb', 'RWB'),
    ('rb', 'RB'),
    ('cb', 'CB'),
    ('lb', 'LB'),
    ('lwb', 'LWB'),
    ('cdm', 'CDM'),
    ('cm', 'CM'),
    ('cam', 'CAM'),
    ('rm', 'RM'),
    ('rw', 'RW'),
    ('rf', 'RF'),
    ('lm', 'LM'),
    ('lw', 'LW'),
    ('lf', 'LF'),
    ('cf', 'CF'),
    ('st', 'ST')
]


class PlayerFilterForm(forms.Form):
    rating_min = forms.ChoiceField(
        choices=large_range_choices,
        initial=1
    )

    rating_max = forms.ChoiceField(
        choices=reversed(large_range_choices),
        initial=99
    )

    player_type = forms.ChoiceField(
        initial='All'
    )

    skill_min = forms.ChoiceField(
        choices=small_range_choices,
        initial=1
    )

    skill_max = forms.ChoiceField(
        choices=reversed(small_range_choices),
        initial=5
    )

    weak_min = forms.ChoiceField(
        choices=small_range_choices,
        initial=1
    )

    weak_max = forms.ChoiceField(
        choices=reversed(small_range_choices),
        initial=5
    )

    workrate_def = forms.ChoiceField(
        choices=workrate_choices,
        initial='All'
    )

    workrate_att = forms.ChoiceField(
        choices=workrate_choices,
        initial='All'
    )

    preferred_foot = forms.ChoiceField(
        initial='All'
    )

    prices_platform = forms.ChoiceField(
        choices=prices_platform_choices,
        initial='Xbox'
    )

    prices_min = forms.CharField(
        min_length=1,
        max_length=8,
        initial=0
    )

    prices_max = forms.CharField(
        min_length=1,
        max_length=8,
        initial=1000000
    )

    positions = forms.MultipleChoiceField(
        choices=position_choices,
        initial='All'
    )

    clubs = forms.ModelChoiceField(
        queryset=Club.objects.all(),
        empty_label='All',
        required=False,
        initial='All'
    )

    leagues = forms.ModelChoiceField(
        queryset=League.objects.all(),
        empty_label='All',
        required=False,
        initial='All'
    )

    nations = forms.ModelChoiceField(
        queryset=Nation.objects.all(),
        empty_label='All',
        required=False,
        initial='All'
    )

    def __init__(self, *args, **kwargs):
        super(PlayerFilterForm, self).__init__()

        player_type_choices = default_all_choice + [
            (x, x.capitalize() if x != 'team of the week' else 'TOTW') for x in
            Player.objects.order_by().values_list(
                'player_type',
                flat=True
            ).distinct()
        ]

        preferred_foot_choices = default_all_choice + [
            (x, x) for x in Player.objects.order_by().values_list(
                'preferred_foot',
                flat=True
            ).distinct()
        ]

        self.fields['player_type'].choices = player_type_choices
        self.fields['preferred_foot'].choices = preferred_foot_choices

