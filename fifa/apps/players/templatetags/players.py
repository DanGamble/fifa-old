from django import template

register = template.Library()


@register.simple_tag()
def stat_color(stat):
    if stat >= 81:
        return 'color--great'
    elif 80 >= stat >= 71:
        return 'color--good'
    elif 70 >= stat >= 61:
        return 'color--average'
    elif 60 >= stat >= 51:
        return 'color--fair'
    else:
        return 'color--poor'


@register.assignment_tag()
def traits(traits):
    return traits.split(',')


@register.assignment_tag()
def specialities(specialities):
    return specialities.split(',')


@register.inclusion_tag('players/includes/stars.html')
def stars(player_skill):
    return {
        'active': range(player_skill),
        'inactive': range(5 - player_skill)
    }
