from django.contrib import admin

from apps.players.models import Player


@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ['common_name', 'overall_rating', 'created', 'modified']
    search_fields = ['common_name']
