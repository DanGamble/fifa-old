from django.db import models
from django.core.urlresolvers import reverse
from django.utils.text import slugify

from datetime import date


class Player(models.Model):
    created = models.DateTimeField(
        auto_now_add=True
    )

    modified = models.DateTimeField(
        auto_now=True
    )

    asset_id = models.PositiveIntegerField(
        db_index=True
    )

    first_name = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )

    last_name = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )

    common_name = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )

    slug = models.SlugField(
        unique=True,
        blank=True,
        null=True
    )

    birth_date = models.DateField(
        blank=True,
        null=True
    )

    overall_rating = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    potential_rating = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    height = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    weight = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    position = models.CharField(
        max_length=5,
        blank=True,
        null=True
    )

    position_line = models.CharField(
        max_length=5,
        blank=True,
        null=True
    )

    preferred_foot = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )

    club = models.ForeignKey(
        'clubs.Club',
        to_field='asset_id',
        blank=True,
        null=True,
        related_name='players'
    )

    league = models.ForeignKey(
        'leagues.League',
        to_field='asset_id',
        blank=True,
        null=True,
        related_name='players'
    )

    nation = models.ForeignKey(
        'nations.Nation',
        to_field='asset_id',
        blank=True,
        null=True,
        related_name='players'
    )

    skill_moves = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    weak_foot = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    workrate_att = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )

    workrate_def = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )

    card_att1 = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    card_att2 = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    card_att3 = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    card_att4 = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    card_att5 = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    card_att6 = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    quality = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )

    color = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )

    is_special = models.BooleanField(
        default=False
    )

    item_type = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )

    acceleration = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    sprint_speed = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    agility = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    balance = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    jumping = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    stamina = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    strength = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    reactions = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    aggression = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    interceptions = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    positioning = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    vision = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    ball_control = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    crossing = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    dribbling = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    finishing = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    free_kick_accuracy = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    heading_accuracy = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    long_passing = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    short_passing = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    marking = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    long_shots = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    shot_power = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    standing_tackle = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    sliding_tackle = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    volleys = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    curve = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    penalties = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    gk_diving = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    gk_handling = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    gk_kicking = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    gk_reflexes = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    gk_positioning = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    traits = models.CharField(
        max_length=500,
        blank=True,
        null=True
    )

    specialities = models.CharField(
        max_length=500,
        blank=True,
        null=True
    )

    player_type = models.CharField(
        max_length=50,
        blank=True,
        null=True
    )

    color = models.CharField(
        max_length=50,
        blank=True,
        null=True
    )

    is_goalkeeper = models.BooleanField(
        default=False
    )

    def __str__(self):
        return self.common_name

    class Meta:
        ordering = ["-overall_rating"]
        verbose_name = "player"
        verbose_name_plural = "players"

    def get_absolute_url(self):
        return reverse('players:player_detail', kwargs={'slug': self.slug})

    def get_card_colour(self):
        # Return a css class to give the player a card background
        return slugify('card-player-{}'.format(self.color)).replace('_', '-')

    def age(self):
        today = date.today()

        try:
            birthday = self.birth_date.replace(
                year=today.year
            )

        except ValueError:
            birthday = self.birth_date.replace(
                year=today.year,
                month=self.birth_date.month + 1,
                day=1
            )

            if birthday > today:
                return today.year - self.birth_date.year - 1
            else:
                return today.year - self.birth_date.year

    def card_name(self):
        return self.common_name.rsplit()[-1]

    def label_card_att1(self):
        return '{}'.format('Diving' if self.is_goalkeeper else 'Pace')

    def label_card_att2(self):
        return '{}'.format('Handling' if self.is_goalkeeper else 'Shooting')

    def label_card_att3(self):
        return '{}'.format('Kicking' if self.is_goalkeeper else 'Passing')

    def label_card_att4(self):
        return '{}'.format('Reflexes' if self.is_goalkeeper else 'Dribbling')

    def label_card_att5(self):
        return '{}'.format('Speed' if self.is_goalkeeper else 'Defending')

    def label_card_att6(self):
        return '{}'.format('Positioning' if self.is_goalkeeper else 'Physicality')

    def label_abbr_card_att1(self):
        return '{}'.format('DIV' if self.is_goalkeeper else 'PAC')

    def label_abbr_card_att2(self):
        return '{}'.format('HAN' if self.is_goalkeeper else 'SHO')

    def label_abbr_card_att3(self):
        return '{}'.format('KIC' if self.is_goalkeeper else 'PAS')

    def label_abbr_card_att4(self):
        return '{}'.format('REF' if self.is_goalkeeper else 'DRI')

    def label_abbr_card_att5(self):
        return '{}'.format('SPD' if self.is_goalkeeper else 'DEF')

    def label_abbr_card_att6(self):
        return '{}'.format('POS' if self.is_goalkeeper else 'PHY')
