# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='position_line',
            field=models.CharField(blank=True, null=True, max_length=5),
            preserve_default=True,
        ),
    ]
