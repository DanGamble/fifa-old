from django.core import management
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        """
        We execute these in a certain order as the prior relies on having a former
        as a Foreign Key, it's slow... for now but it's how it has to be done
        """
        management.call_command('pull_nations')
        management.call_command('pull_leagues')
        management.call_command('pull_clubs')
        management.call_command('pull_players')
