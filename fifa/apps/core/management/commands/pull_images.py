from django.core.management.base import BaseCommand, CommandError

from apps.core.functions.lets_get_that_data import lets_get_dem_images


class Command(BaseCommand):

    def handle(self, *args, **options):
        lets_get_dem_images()
