from django.core.management.base import BaseCommand, CommandError
from django.db.models import Avg
from django.db.models.loading import get_model

from apps.players.models import Player

models = {
    'club': {
        'app': 'clubs',
        'player_fk': 'club'
    },
    'league': {
        'app': 'leagues',
        'player_fk': 'league'
    },
    'nation': {
        'app': 'nations',
        'player_fk': 'nation'
    }
}


class Command(BaseCommand):

    def handle(self, *args, **options):

        for arg in args:
            arg = arg.lower()
            model = get_model(models[arg]['app'], arg.lower())

            for item in model.objects.all():

                queryset = Player.objects.filter(
                    **{models[arg]['player_fk']: item.asset_id}
                )

                item.players_count = queryset.count()

                item.players_average_rating = queryset.aggregate(
                    Avg('overall_rating')
                )['overall_rating__avg']

                item.players_inform = queryset.exclude(
                    player_type__in=['rare', 'standard']
                ).count()

                item.players_gold = queryset.filter(
                    color__in=['gold', 'rare_gold']
                ).count()

                item.players_silver = queryset.filter(
                    color__in=['silver', 'rare_silver']
                ).count()

                item.players_bronze = queryset.filter(
                    color__in=['bronze', 'rare_bronze']
                ).count()

                item.players_gk = queryset.filter(
                    position_line='GK'
                ).count()

                item.players_def = queryset.filter(
                    position_line='DEF'
                ).count()

                item.players_mid = queryset.filter(
                    position_line='MID'
                ).count()

                item.players_att = queryset.filter(
                    position_line='ATT'
                ).count()

                item.save()
