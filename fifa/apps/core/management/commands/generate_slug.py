from django.core.management.base import BaseCommand, CommandError
from django.db.models.loading import get_model
from django.utils.text import slugify


class Command(BaseCommand):

    def handle(self, *args, **options):
        Player = get_model('players', 'Player')

        for player in Player.objects.all():
            player.slug = slugify('{}-{}'.format(player.id, player.common_name))
            player.save()
