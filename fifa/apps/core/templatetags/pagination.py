from django import template

register = template.Library()


@register.inclusion_tag('includes/detail_pagination.html')
def detail_pagination(qs):
    return{
        'qs': qs
    }


@register.inclusion_tag('includes/list_pagination.html', takes_context=True)
def list_pagination(context):
    return{
        'is_paginated': context['is_paginated'],
        'page_obj': context['page_obj']
    }
