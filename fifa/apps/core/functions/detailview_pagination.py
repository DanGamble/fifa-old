from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


def detailview_pagination(self, query, per_page=28):

    # Create pagination for the players return
    paginator = Paginator(query, per_page)
    # Get the page from the URL
    page = self.request.GET.get('page')

    try:
        # Deliver the requested page
        pagination = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        pagination = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        pagination = paginator.page(paginator.num_pages)

    return pagination
