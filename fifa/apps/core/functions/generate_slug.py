from django.utils.text import slugify

from apps.players.models import Player


def generate_slug(obj):
    player = Player.objects.get(id=obj.id)

    player.slug = slugify('{}-{}'.format(player.id, player.common_name))
    player.save()
