# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('clubs', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='club',
            options={'verbose_name_plural': 'clubs', 'verbose_name': 'club', 'ordering': ['-players_average_rating', 'name']},
        ),
        migrations.AlterField(
            model_name='club',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 15, 18, 12, 18, 781349, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='club',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 15, 18, 12, 27, 421815, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
