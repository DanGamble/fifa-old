# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('leagues', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Club',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('asset_id', models.PositiveIntegerField(unique=True, db_index=True)),
                ('name', models.CharField(max_length=100, blank=True, null=True)),
                ('name_abbr', models.CharField(max_length=100, blank=True, null=True)),
                ('created', models.DateTimeField(null=True, auto_now_add=True)),
                ('modified', models.DateTimeField(null=True, auto_now=True)),
                ('slug', models.CharField(max_length=50, blank=True)),
                ('players_count', models.PositiveIntegerField(blank=True, null=True)),
                ('players_average_rating', models.PositiveIntegerField(blank=True, null=True)),
                ('players_inform', models.PositiveIntegerField(blank=True, null=True)),
                ('players_gold', models.PositiveIntegerField(blank=True, null=True)),
                ('players_silver', models.PositiveIntegerField(blank=True, null=True)),
                ('players_bronze', models.PositiveIntegerField(blank=True, null=True)),
                ('players_gk', models.PositiveIntegerField(blank=True, null=True)),
                ('players_def', models.PositiveIntegerField(blank=True, null=True)),
                ('players_mid', models.PositiveIntegerField(blank=True, null=True)),
                ('players_att', models.PositiveIntegerField(blank=True, null=True)),
                ('league', models.ForeignKey(related_name='clubs', to='leagues.League', to_field='asset_id')),
            ],
            options={
                'verbose_name_plural': 'clubs',
                'verbose_name': 'club',
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
    ]
