from django.views.generic import ListView, DetailView
from apps.players.models import Player

from apps.core.functions.detailview_pagination import detailview_pagination
from .models import Club


class ClubList(ListView):
    model = Club
    paginate_by = 50


class ClubDetail(DetailView):
    model = Club

    def get_context_data(self, **kwargs):
        context = super(ClubDetail, self).get_context_data()

        players = Player.objects.filter(
            club=self.get_object()
        ).select_related(
            'club',
            'league',
            'nation'
        )

        context['players'] = detailview_pagination(self, players)

        return context
