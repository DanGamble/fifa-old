from django.core.urlresolvers import reverse
from django.db import models


class Club(models.Model):
    created = models.DateTimeField(
        auto_now_add=True
    )

    modified = models.DateTimeField(
        auto_now=True
    )

    asset_id = models.PositiveIntegerField(
        unique=True,
        db_index=True
    )

    name = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )

    name_abbr = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )

    league = models.ForeignKey(
        'leagues.League',
        to_field='asset_id',
        related_name='clubs'
    )

    slug = models.CharField(
        max_length=50,
        blank=True
    )

    players_count = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_average_rating = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_inform = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_gold = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_silver = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_bronze = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_gk = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_def = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_mid = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    players_att = models.PositiveIntegerField(
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name_abbr

    class Meta:
        ordering = ['-players_average_rating', 'name']
        verbose_name = 'club'
        verbose_name_plural = 'clubs'

    def get_absolute_url(self):
        return reverse('clubs:club_detail', kwargs={'slug': self.slug})
