'use strict';

function bundleJspm(gulp, plugins, config) {
    return plugins.shell.task([
        'jspm bundle-sfx ' + config.src.js + 'main ' + config.build.js + 'main.js'
    ]);
}

module.exports = {
    'bundleJspm': bundleJspm
};
