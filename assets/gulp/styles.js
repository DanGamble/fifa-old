'use strict';

var browserSync = require('browser-sync');

function styles(gulp, plugins, config) {
    return function() {
        gulp.src([
            config.src.scss + '*.scss',
            config.src.scss + '**/*.scss'
        ], {base: config.src.scss})
            //.pipe(plugins.sourcemaps.init())
            .pipe(plugins.sass({
                errLogToConsole: true,
                outFile: config.src.css + 'screen.css',
                outputStyle: 'compressed',
                precision: 10,
                sourceMap: config.src.css,
                stats: true
            }))
            .on('error', function(e) {
                console.log(e);
            })
            .pipe(plugins.autoprefixer())
            .pipe(plugins.pxtorem())
            //.pipe(plugins.sourcemaps.write('.'))
            .pipe(gulp.dest(config.build.css))
            .pipe(browserSync.reload({stream: true}))
            .pipe(plugins.size({title: 'styles'}));
    };
}

module.exports = {
    'styles': styles
};
