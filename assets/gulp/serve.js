'use strict';

var browserSync = require('browser-sync');

function serve(gulp, config) {
    return function () {
        browserSync({
            notify       : false,
            logPrefix    : 'FIFA',
            injectChanges: true,
            open         : false,
            proxy        : '0.0.0.0:8000'
        });

        gulp.watch(config.src.js + '**/*.js', ['scripts:bundleJspm']);
        gulp.watch(config.src.scss + '**/*.scss', ['styles']);
    };
}

module.exports = {
    'serve': serve
};
