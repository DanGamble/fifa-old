"use strict";

import $ from 'jquery';

$(function() {
    $('.form__field-wrapper').click(function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.form__field-wrapper.active').not(this).removeClass('active');
            $(this).addClass('active');
        }
    });

    $('.form__field-wrapper .controls').click(function(e) {
        e.stopPropagation();
    });

    $('.form__field-wrapper .controls select').on('change', function() {
        $(this).closest('.form__field-wrapper').find('span').eq($(this).index()).html($(this).val());
        $(this).first().parents().eq(1).removeClass('active');
    });

    $('.form__field-wrapper .controls input').on('change', function() {
        $(this).closest('.field-wrapper').find('span').eq($(this).index()).html($(this).val());
        $(this).first().parents().eq(1).removeClass('active');
    });

    console.log('Bye lol now');
});
